#!/usr/bin/perl

use strict;
use warnings;
no warnings 'uninitialized';
use Data::Dumper;

use lib '/home/wolf/open_source/recognizer/lib';

#use Data::RecordStore::Redis;
use Data::ObjectStore;

use Recognizer;
use Recognizer::Node;
use Web::Scraper;
use WWW::Mechanize;
use Encode;
use Statistics::Basic qw(:all);

#my $ans = combos(qw(0 1 1 0 1 0 1 0 1));
#print STDERR join("\n",sort map { join(",",@$_) } @$ans)."\n";
#map {print STDERR Data::Dumper->Dump([join(",",@$_)]); } @$ans;
#exit;


my( $os, $exp, $exems, $word_nodes, $pair_nodes, $pair_lookup, $w2idx );

init(); # opens the object store, loads/creates experiment and experiment fields

print STDERR "There are ".scalar(@$exems)." exemplar articles loaded\n";
print STDERR "There are ".scalar(@$word_nodes)." word nodes\n";
print STDERR "There are ".scalar(@$pair_nodes)." pair nodes\n";

load_articles( 2530 );

# note the word exemplar count and word to word frequencies
process_words_exemplars();

# finds the word to word sigmas
calc_word_nodes();

# categories are sort of a digest of the exemplars.

# find the strongest knodes common among jnodes
# in an attempt to build up categories.
# the oomkiller sometimes kills this.
calc_categories( 10 );
exit;

build_pairs();

process_pairs_exemplars();

calc_pair_nodes();

print STDERR "Done\n";

exit;

sub init {
    my $clear = shift;
    # opens store and pulls out the experiment objects
#    my $redis_coord_addy = "127.0.0.1:6380";
#    my $redis_cache_addy = "127.0.0.1:6379";

    print STDERR "Opening Provider\n";
    # my $dataprovider = Data::RecordStore::Redis->open_store( RECORD_STORE_PATH    => "/home/wolf/open_source/recognizer/data/RECORDSTORE",
    #                                                          RECORD_STORE_MODE    => 'SINGLE',
    #                                                          REDIS_LOCAL_CACHE => $redis_cache_addy,
    #                                                          REDIS_LOCAL_COORD => $redis_coord_addy,
    #                                                          REDIS_CACHE_SIZE  => '8g',
    #                                                      );
    # if( $clear ) {
    #     print STDERR "Clearing cache\n";
    #     $dataprovider->clear_cache;
    # }


    # print STDERR "Opening Store\n";
    # $os = Data::ObjectStore->open_store( DATA_PROVIDER => $dataprovider,
    #                                      NO_AUTO_CLEAN => 1,
    #                                      CACHE         => 900,
    #                                    );

    $os = Data::ObjectStore->open_store( DATA_PROVIDER => "/home/wolf/open_source/recognizer/data", NO_AUTO_CLEAN => 1, CACHE => 9000 );

    my $root = $os->load_root_container;
    $exp = $root->get_experiment_recognizer_one;

    unless( $exp ) {
        $exp = $os->create_container( 'Recognizer' );
        $root->set_experiment_recognizer_one( $exp );
    }

    $exems = $exp->get_exemplars([]);

    $w2idx       = $exp->get_w2idx({});
    $word_nodes  = $exp->get_word_nodes([]);
    # a word node has
    #   type    => 'word'
    #   payload => the word
    #   idx     => idx in $word_nodes
    #   exem_count => number of exemplars this was in
    #   idx2sigma => other word idx -> sigma that other word has to this word
    #   idx2count => other word idx -> coincidnece count
    #   reverse_idx2sigma => other word idx -> sigma this word has to that other word
    #

    $pair_nodes  = $exp->get_pair_nodes([]);
    # a pair node has
    #   type    => 'pair'
    #   payload => [ nodeidx, nodeidx ]
    #   idx     => idx in $pair_nodes
    #   word_nodes => word nodes list
    #   exem_count => number of exemplars this was in
    #   idx2sigma => other word idx -> sigma that other word has to this word
    #   idx2count => other word idx -> coincidnece count
    #   reverse_idx2sigma => other word idx -> sigma this word has to that other word
    #   sigma => sigma between the two pairs

    # x-idx_i -> x-idx_j -> node (x is 'w' for word node, 'p' for pair node)
    $pair_lookup = $exp->get_pair_lookup({});
} #init

##
## make sure there are so many random articles from wikipedia stored
##   each article has a uri and a list of paragraphs which are
##   utf-8 encoded blocks of text.
##
sub load_articles {
    my $count = shift;
    if( @$exems >= $count ) {
        return;
    }

    my $paras = scraper {
        process 'body', 'bod' => 'TEXT',
            process '.mw-parser-output > p', 'paragraphs[]' => 'TEXT';
    };
    my $mech = WWW::Mechanize->new();

    my $loaded;

    print STDERR "Loading ".($count-@$exems)." articles to bring the total to $count.\n";

    my $scount;
    while( @$exems < $count ) {
        sleep(1);
                
        my $url = 'https://en.wikipedia.org/wiki/Special:Random';
        print STDERR "Loading Random Page\n";
        $mech->get( $url );
        my $uri = $mech->uri()->as_string;

        my $html = $mech->content( decoded_by_headers => 1);

        my $res = $paras->scrape( $html );

        my $paras = [map { Encode::encode( 'utf8', $_ ) } grep { /\S/ } @{$res->{paragraphs}||[]}];

        if( @$paras < 3 ) {
            print STDERR "Skipping, not enough paragraphs\n";
            next;
        }

        print STDERR "Page loaded\n";

        my $exem = $os->create_container( {
            uri        => Encode::encode( 'utf8', $uri ),
            paragraphs => $paras,
        } );

        push @$exems, $exem;
        process_words_exemplar( $exem );  #this marks the exemplar as 'done' for word nodes processing but not pair processing
        $exp->set_exem_idx_processed_words( $#$exems );
        if( ++$scount > 500 ) {
            $os->save;
            $scount = 0;
        }
        if( $os->pending > 20_000_000_000 ) {
            print STDERR "Syncing to disc...\n";
            $os->sync;
            print STDERR "Done\n";
        }

        print STDERR "\n$uri\n\n".join("\n----\n",@{$exem->get_paragraphs})."\n\n(".scalar(@$exems)."/$count)\n\n";
        $loaded = 1;
    }

    print STDERR "Syncing articles.\n";
    $os->sync;
    print STDERR "Done syncing articles.\n";
} #load_articles

# returns a list reference containing the node objects corresponding to the
# given text
sub text_nodes {
    my $txt = lc( shift );

    $txt =~ s/[^a-z\s]+//gs;

    my( %words ) = map { $_ => 1 } grep { length($_) > 3 } split( /\s+/, $txt );
    my $ret = [];
    for my $word ( keys %words ) {
        my $idx = $w2idx->{$word};
        unless( $idx ) {
            $idx = @$word_nodes;
            $w2idx->{$word} = $idx;
            my $node = $os->create_container( 'Recognizer::Node',
                                            {
                                                type    => 'word',
                                                payload => $word,
                                                idx     => $idx,
                                            },  );
            push @$word_nodes, $node;
        }
        push @$ret, $word_nodes->[$idx];
    }
    return $ret;
} #text_nodes

sub process_words_exemplars {

    my $top_exem_idx = $exp->get_exem_idx_processed_words;

    print STDERR "Processing exemplars for words\n";

    if( $#$exems > $top_exem_idx ) {
        my $scount;
        for( my $i=$top_exem_idx; $i<@$exems; $i++ ) {
            my $exe = $exems->[$i];
            process_words_exemplar( $exe );
            if( ++$scount > 500 ) {
                $os->save;
                $scount = 0;
            }
            if( $os->pending > 20_000_000_000 ) {
                print STDERR "Syncing to disc...\n";
                $os->sync;
                print STDERR "Done\n";
            }
        } #each exemplar
    }

    print STDERR "Done processing exemplars for words\n";
    $exp->set_exem_idx_processed_words( $#$exems );
    print STDERR "Syncing after processing word exemplars\n";
    $os->save;
    $os->sync;
    print STDERR "Done syncing\n";

} #process_words_exemplars

#
# look at the paragraphs in the exemplar. increment
# the word to word counts and the exemplar counts
# for the words
#
sub process_words_exemplar {
    my $exe = shift;

    my $paragraphs = $exe->get_paragraphs;
    print STDERR "Processing ".$exe->get_uri."\n";
    print STDERR "  Got ".scalar(@$paragraphs)." paragraphs\n";

    for my $para (@$paragraphs) {
        # get the words
        my $nodes = text_nodes( $para );
        print "Got ".scalar(@$nodes)." text nodes\n";
        for ( my $i=0; $i<$#$nodes; $i++ ) {
            my $aword = $nodes->[$i];
            $aword->set_exem_count( 1 + $aword->get_exem_count(0) );
            my $aidx = $aword->get_idx2count;
            for ( my $j=$i+1; $j<@$nodes; $j++ ) {
                my $bword = $nodes->[$j];
                $aidx->{$bword->get_idx}++;
                $bword->get_idx2count->{$aword->get_idx}++;
            }
        }
    }

} #process_words_exemplar

#
# Returns all the 'orable' combinations of the input array
# that means, any of the results ORed with the initial input
# will procoduce the initial input
#
no warnings 'recursion';
sub combos {
    my( $first, @rest ) = @_;
    if( 0 == @rest ) {
        return $first ? [[0],[$first]] : [[0]];
    }
    my $combos = combos(@rest);
    return $first ? [ map { [$first,@$_],[0,@$_] } @$combos ] :
        [ map { [0,@$_] } @$combos ];
} #combos

sub calc_categories {
    my $min = shift || 12;
    # this is the story of categories spawned by
    # each word node.
    #
    # the word node in question is the 'i' node.
    # the 'i' node has jnodes associated with it through sigma (and reverse sigma)
    # calculations.
    #
    # in turn, each jnode as knodes associated with it through sigma (and reverse sigma)
    # calculations.
    #
    # doing the calc categories finds the groupings of knodes most common among the jnodes
    #
    
    for( my $i=0; $i<@$word_nodes; $i++ ) {
        my $inode = $word_nodes->[$i];
#        print STDERR $inode->payload."\n";
        # return if too small or updated recently
        if( $inode->get_exem_count > 40 ) {
            my $important_to_others_sigmas = $inode->get_reverse_idx2sigma({});
            my $important_to_me_sigmas     = $inode->get_idx2sigma({});

            my $okeys = [keys %$important_to_others_sigmas];
            my $mkeys = [keys %$important_to_me_sigmas];

            print STDERR $inode->get_payload . ' ' . $inode->get_exem_count . " <=== " .join (" ", map { $word_nodes->[$_]->get_payload } sort { $important_to_others_sigmas->{$b} <=> $important_to_others_sigmas->{$a} } @$okeys ) . "\n";
            print STDERR $inode->get_payload . ' ' . $inode->get_exem_count . " ===> " .join (" ", map { $word_nodes->[$_]->get_payload } sort { $important_to_me_sigmas->{$b} <=> $important_to_me_sigmas->{$a} } @$mkeys ) . "\n";

            my $rcount = 0;

            # obits is a list of word idxes.
            # omap map a word idx to position in the obits list.
            my( $obits, $mbits ) = ( [], [] );
            my( $omap, $mmap)  = ( {}, {} );
            my( $ocount, $mcount ) = ( {}, {} );
            my( $oclusters, $mclusters ) = ( {}, {} );
            
            for my $jparts ( ["Reverse Sigma j clusters",$okeys ], ["Sigma j clusters",$mkeys ] ) {
                my( $jtitle, $jset ) = @$jparts;
                print STDERR "Processing $jtitle ".scalar(@$jset)."\n";
                for my $jkey (@$jset) {
                    my $jnode = $word_nodes->[$jkey];
                    
                    my $m_sig = $jnode->get_idx2sigma({});
                    my $o_sig = $jnode->get_reverse_idx2sigma({});
                    for my $parts ( [$o_sig,$ocount], [$m_sig,$mcount] ) {
                        my( $sig, $cnt ) = @$parts;
                        for my $k (keys %$sig) {
                            $cnt->{$k}++;
                        } #each k
                    }
                }

                #should have at least 40%
                my( @okeys ) = keys %$ocount;
                my( @mkeys ) = keys %$mcount;
                my $obase = int( @okeys * .4 );
                $obase = $obase < $min ? $min : $obase;
                my $mbase = int( @mkeys * .4 );
                $mbase = $mbase < $min ? $min : $mbase;

                $obits = [sort { $ocount->{$b} <=> $ocount->{$a} || $b <=> $a } grep { $ocount->{$_} > $obase } keys %$ocount];
                $mbits = [sort { $mcount->{$b} <=> $mcount->{$a} || $b <=> $a } grep { $mcount->{$_} > $mbase } keys %$mcount];
                
                for my $jkey (@$jset) {
                    my $jnode = $word_nodes->[$jkey];
                    
                    my $m_sig = $jnode->get_idx2sigma({});
                    my $o_sig = $jnode->get_reverse_idx2sigma({});
                    for my $parts ( [$o_sig,$obits,$oclusters], [$m_sig,$mbits,$mclusters] ) {
                        my( $sig, $bits, $cluster ) = @$parts;
                        next unless @$bits;
                        my( @vbits ) = map { $sig->{$_} ? $_ : 0 } @$bits;
                        my $vsize = grep { $_ } @vbits;
#                        print STDERR " **Calculating ".scalar(@vbits)." bits of size $vsize **\n";
                        my $combos = combos( @vbits );
                        for my $combo (@$combos) {
                            if( 2 < grep { $_ } @$combo ) {
                                $cluster->{ join(",",@$combo) }++;
                            }
                        }
                    }
                }

                for my $parts ( ["Reverse Sigma k clusters", $oclusters], ["Sigma k clusters", $mclusters] ){
                    my( $title, $clusters ) = @$parts;
                    for my $cluster (sort { $clusters->{$b} <=> $clusters->{$a} } grep { $clusters->{$_} > 3 } keys %$clusters) {
                        my( @parts ) = grep { $_ } split( /,/, $cluster );
                        next unless @parts > 5;
                        print "$jtitle | $title : ($clusters->{$cluster}) "
                            . join(' ', map { $word_nodes->[$_]->payload }
                                   @parts )
                            ."\n";
                    }
                }
            } #each j
        } #inode valid
    } #each inode
} #calc_categories

sub calc_word_nodes {
    my $force = shift;
return;
    print "Calc word nodes called\n";
    my $updated = 0;
    print "Caching node info\n";
    my( @cache ) = map { [$_,$_->get_idx2count,$_->get_idx2sigma,$_->get_reverse_idx2sigma] } @$word_nodes;
    print "Starting loop\n";
    for( my $i=0; $i<@$word_nodes; $i++ ) {
        my $inode = $word_nodes->[$i];
        # return if too small or updated recently
        if( $inode->get_exem_count > 40 ) {
            my $iidx = $inode->get_idx;

            my( $freqs );
            for ( my $j=0; $j<@$word_nodes; $j++ ) {
                next if $i==$j;
                my $jnode = $word_nodes->[$j];

                my $jseen = $jnode->get_exem_count;
                next unless $jseen > 40;

                # calculate the average frequency with other cues
                $freqs->[$j] = $jseen ? $jnode->get_idx2count->{$iidx} / $jseen : 0;
            }

            # calc avg from freqs, then the std for each
            my $avg = mean( [grep { defined } @$freqs] );
            my $v = $avg->query_vector;
            my $stdev = stddev( $v ) + 0;

#            print STDERR "Average for ($iidx) std ($stdev) : $avg\n";

            $inode->set_stdev( $stdev );
            $inode->set_avg( 0 + $avg );
            my $isig = $inode->get_idx2sigma;
            
            if ( $stdev ) {
                for ( my $j=0; $j<@$word_nodes; $j++ ) {
                    next unless defined $freqs->[$j];

                    # if the i word is significant to the j word
                    # update both nodes
                    my $sigma = ($freqs->[$j] - $avg)/$stdev;
                    if ( abs( $sigma ) > 3 ) {
                        my $affinity = $sigma * $inode->get_exem_count;
                        my $jnode = $word_nodes->[$j];
                        $jnode->get_reverse_idx2sigma->{$inode->get_idx} = $affinity;
                        $isig->{$jnode->get_idx} = $affinity;
                    }
                }
            }
            $inode->set_last_updated( time );
            $updated = 1;

            if ( scalar( keys %$isig ) ) {
                print STDERR $inode->get_payload . " => " .join (" ", map { $word_nodes->[$_]->get_payload } sort { $isig->{$b} <=> $isig->{$a} } keys %$isig ) . "\n";
            }
        } #if word qualifies
    }

    if( $updated ) {
        $exp->set_last_updated_nodes( time );
    }

    print STDERR "Syncing after calc word nodes\n";
    $os->save;
    $os->sync;
    print STDERR "Done syncing\n";

} #calc_word_nodes

# payload of node
sub pl {
    return $word_nodes->[shift]->get_payload;
}

sub build_cats {

    print "Calc Word Nodes\n";

    #matching every word node with every other.
    for( my $i=0; $i<@$word_nodes; $i++ ) {
        my $inode = $word_nodes->[$i];
        my $r = $inode->get_idx2sigma;

        if( $r ) {
            for my $rel_idx (keys %$r) {
                my $rel_node = $word_nodes->[$rel_idx];
                # make a 'category' from each of these paired with the first node.
                my $sr = $rel_node->get_idx2sigma;
                my( %idx2sigma );
                for my $sec_idx (keys %$sr) {
                    next if $sec_idx == $i;
                    my $s = $r->{$sec_idx} < $sr->{$sec_idx} ? $r->{$sec_idx} : $sr->{$sec_idx};
                    if( $s > 3 ) {
                        $idx2sigma{$sec_idx} = $s;
                    }
                }
                if( scalar(keys(%idx2sigma)) > 3 ) {
                    print join( " ", pl( $i ), pl( $rel_idx ),
                                map { pl($_)}
                                    sort { $idx2sigma{$b} <=> $idx2sigma{$a} } keys %idx2sigma )
                        ."\n";
                }
            }
        }
    }


} #build_cats


sub build_pairs {
    my( $force ) = @_;

    if( $exp->get_last_updated_nodes < $exp->get_pairs_built_time && ! $force ) {
        return;
    }

    if( $force ) {
        $pair_lookup = {};
        $exp->set_pair_lookup($pair_lookup);
        splice @$pair_nodes;
    }
    print STDERR "Building Pairs\n";

    # pair_nodes contains word_i -> { word_j -> pair_object }
    #  a pair_object will be stored twice, also word_j -> { word_i -> pair_object }
    my $built = 0;

    for( my $i=0; $i<@$word_nodes; $i++ ) {
        my $inode = $word_nodes->[$i];
        if( $inode->get_exem_count >= 50 ) {
            $built = 1;
            my $iword = $inode->get_payload;

            my $sigmas     = $inode->get_idx2sigma;          # index -> sigma
            my $rev_sigmas = $inode->get_reverse_idx2sigma;  # index -> sigma
            my $i_exems    = $inode->get_exem_count;

            print STDERR "Building $iword -> ".join(' ',map { $word_nodes->[$_]->get_payload } sort { $rev_sigmas->{$b} <=> $rev_sigmas->{$a} } keys %$rev_sigmas )."\n";
            for my $j (keys %$rev_sigmas) {
                if( $j > $i ) {  # the order of the pair does not matter, so if the $j is less than the $i, that $h has been processed
                    my $jnode = $word_nodes->[$j];
                    if( $jnode->get_exem_count >= 50 ) {
                        my $sigma = $jnode->get_exem_count > $i_exems ? $rev_sigmas->{$j} : $sigmas->{$j};
                        my $jword = $jnode->get_payload;
                        my $idx = @$pair_nodes;
                        my $node = $os->create_container( 'Recognizer::Node',
                                                        {
                                                            type    => 'pair',
                                                            word_nodes => $word_nodes,
                                                            payload => [$i,$j],
                                                            idx     => $idx,
                                                            sigma   => $sigma,
                                                        }  );
                        $pair_lookup->{ $iword }{ $jword } = $node;
                        $pair_lookup->{ $jword }{ $iword } = $node;
                        push @$pair_nodes, $node;
                    }
                }
            } #each j word node
        } #condition
    } #each i word node

    if( $built ) {
        $exp->set_pairs_built_time( time );
        print STDERR "Syncing after build pairs\n";
        $os->save;
        $os->sync;
        print STDERR "Done Syncing\n";
    }

} #build_pairs


sub process_pairs_exemplars {
    my $force = shift;

    my $top_exem_idx = $exp->get_exem_idx_processed_pairs;

    print STDERR "Processing Pairs\n";

    if( $force ) {
        $top_exem_idx = 0;
    }

    my $scount = 0;
    if( $#$exems > $top_exem_idx ) {
        for( my $i=$top_exem_idx; $i<@$exems; $i++ ) {
            my $exe = $exems->[$i];
            process_pairs_exemplar( $exe, $force );
        } #each exemplar
    }
    $exp->set_exem_idx_processed_pairs( $#$exems );
    print STDERR "Syncing after process pairs\n";
    $os->save;
    $os->sync;
    print STDERR "Synced\n";

} #process_pairs_exemplars

# should not call this when injesting exemplars
# in case the word nodes havn't been examined into
# pairs yet
sub process_pairs_exemplar {
    my( $exe, $force ) = @_;

    return if $exe->get_is_pairs_done && ! $force;

    my $paragraphs = $exe->get_paragraphs;

    my( @all_pairs );
    for my $para (@$paragraphs) {
        # get the words
        print STDERR "\n\n$para\n\n";
        my $nodes = text_nodes( $para );

        # see which pair nodes these correspond to. A word may be used only once for a pair node,
        # but will use the strongest score.

        my( @pair_cands );

        my( @idxs ) = sort { $a <=> $b } map { $_->get_idx } @$nodes;
        for ( my $i=0; $i < $#idxs; $i++ ) {
            my $i_idx = $idxs[$i];
            my $i_word_node = $word_nodes->[$i_idx];
            my $i_word = $i_word_node->get_payload;

            # make sure this word has a pair candidate
            my $i_pairs = $pair_lookup->{$i_word};

            if ( $i_pairs ) {
#                print STDERR Data::Dumper->Dump([$i_word,join(" ",map { $_->payload } values %$i_pairs),"IP"]);
                for ( my $j=$i+1; $j < @idxs; $j++ ) {
                    my $j_idx = $idxs[$j];
                    my $j_word_node = $word_nodes->[$j_idx];
                    my $j_word = $j_word_node->get_payload;
                    my $i_j_node = $i_pairs->{$j_word};
#                    print STDERR "\t$j_word\n";
                    if ( $i_j_node ) {
#                        print STDERR $i_j_node->payload.' : '.$i_j_node->get_sigma."\n";
                        push @pair_cands, $i_j_node;
                        push @all_pairs, $i_j_node;
                    }
                }
            }
        } # each pairable word in the paragraphs

        # a word may only be paired once, so find the strongest pairings
        @pair_cands = sort { $b->get_sigma <=> $a->get_sigma } @pair_cands;
        my( %seen, @pairs );
        for my $pair (@pair_cands) {
            my( $a, $b ) = @{$pair->get_payload};
            next if $seen{$a} || $seen{$b};
            $seen{$a} = 1;
            $seen{$b} = 1;
            push @pairs, $pair;
            $pair->set_exem_count( 1 + $pair->get_exem_count );
        }

        print STDERR "PAIRS : ". join(" ", map { $_->payload } @pairs )."\n";

        # associate the resulting pairs
        for ( my $i=0; $i<$#pairs; $i++ ) {
            my $apair = $pairs[$i];
            for ( my $j=$i+1; $j<@pairs; $j++ ) {
                my $bpair = $pairs[$j];
                $apair->get_idx2count->{$bpair->get_idx}++;
                $bpair->get_idx2count->{$apair->get_idx}++;
            }
        }
    }

    my @pair_cands = sort { $b->get_sigma <=> $a->get_sigma } @all_pairs;
    my( %seen, @pairs );
    for my $pair (@pair_cands) {
        my( $a, $b ) = @{$pair->get_payload};
        next if $seen{$a} || $seen{$b};
        $seen{$a} = 1;
        $seen{$b} = 1;
        push @pairs, $pair;
        $pair->set_exem_count( 1 + $pair->get_exem_count );
    }

    print STDERR "\nFULL PAIRS : ". join(" ", map { $_->payload.' '.$_->get_exem_count } @pairs )."\n";

    for ( my $i=0; $i<$#pairs; $i++ ) {
        my $apair = $pairs[$i];
        for ( my $j=$i+1; $j<@pairs; $j++ ) {
            my $bpair = $pairs[$j];
            $apair->get_idx2count->{$bpair->get_idx}++;
            $bpair->get_idx2count->{$apair->get_idx}++;
        }
    }
    $exe->set_is_pairs_done(1);
} #process_pairs_exemplar

sub calc_pair_nodes {
    my $force = shift;
    my $updated = 0;

    my $top_exem_idx = $exp->get_pair_processed_idx(0);

    if( $force ) {
        $top_exem_idx = 0;
    }

    if( $top_exem_idx < $#$pair_nodes ) {
        print STDERR "calculating pair nodes\n";

        for ( my $i=$top_exem_idx; $i<@$pair_nodes; $i++ ) {
            my $inode = $pair_nodes->[$i];
            print STDERR $inode->payload . " -> " . $inode->get_exem_count . "\n";
            # return if too small or updated recently
            unless( $inode->get_exem_count < 50 || ( ! $force && $inode->get_last_updated < $exp->get_last_updated_nodes ) ) {
                my $iidx = $inode->get_idx;

                my( $freqs );
                for ( my $j=0; $j<@$pair_nodes; $j++ ) {
                    next if $i==$j;
                    my $jnode = $pair_nodes->[$j];

                    my $jseen = $jnode->get_exem_count;
# next if $jseen < 50;
                    # calculate the average frequency with other cues. Zero is permitted
                    # since this is part of the averaging
                    $freqs->[$j] = $jseen ? $jnode->get_idx2count->{$iidx} / $jseen : 0;
                }

                # calc avg from freqs, then the std for each
                my $avg = mean( [grep { defined } @$freqs] );
                my $v = $avg->query_vector;
                my $stdev = stddev( $v ) + 0;

                print STDERR "Average for ($iidx) std ($stdev) : $avg\n";

                $inode->set_stdev( $stdev );
                $inode->set_avg( 0 + $avg );
                if ( $stdev ) {
                    for ( my $j=0; $j<@$pair_nodes; $j++ ) {
                        next unless defined $freqs->[$j];

                        # if the i pair is significant to the j pair
                        # update both nodes
                        my $sigma = ($freqs->[$j] - $avg)/$stdev;
                        if ( abs( $sigma ) > 3 ) {
                            my $jnode = $pair_nodes->[$j];
                            $jnode->get_idx2sigma->{$inode->get_idx} = $sigma * $inode->get_exem_count;
                            $inode->get_reverse_idx2sigma->{$jnode->get_idx} = $sigma * $jnode->get_exem_count;
                        }
                    }
                }
                $inode->set_last_updated( time );
                $updated = 1;

                my $r = $inode->get_reverse_idx2sigma;
                if ( scalar( keys %$r ) ) {
                    print STDERR $inode->payload . "  : " .join (" ", map { $pair_nodes->[$_]->payload } sort { $r->{$b} <=> $r->{$a} } keys %$r ) . "\n";
                }
                $r = $inode->get_idx2sigma;
                if ( scalar( keys %$r ) ) {
                    print STDERR $inode->payload . "  : " .join (" ", map { $pair_nodes->[$_]->payload } sort { $r->{$b} <=> $r->{$a} } keys %$r ) . "\n\n";
                }
            } #if pair qualifies
        }
    } #if idx was needed
    print STDERR Data::Dumper->Dump(["NOSAVE"]);exit;
    $exp->set_pair_processed_idx($#$pair_nodes);
    if( $updated ) {
        $exp->set_last_updated_nodes( time );
    }
    print STDERR "syncing after calculating pair nodes\n";
    $os->save;
    $os->sync;
    print STDERR "synced\n";
} #calc_pair_nodes


__END__
