package Recognizer::Node;

use strict;
use warnings;

use Data::ObjectStore;

use base 'Data::ObjectStore::Container';

sub _init {
    my $self = shift;
    $self->set_idx2count({});
    $self->set_idx2sigma({});
    $self->set_reverse_idx2sigma({});
}

sub payload {
    my $self = shift;
    if( $self->get_type eq 'pair' ) {
        my $word_nodes = $self->get_word_nodes;
        my( $i, $j ) = @{$self->get_payload};
        return "[".$word_nodes->[$i]->payload.' '.$word_nodes->[$j]->payload."]";
    }
    return $self->get_payload;
}

1;
