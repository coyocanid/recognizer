package Recognizer;

use strict;
use warnings;

use Data::ObjectStore;

use base 'Data::ObjectStore::Container';

sub _init {
    my $self = shift;
    $self->set_pair_nodes([]);
    $self->set_pair_lookup({});
}


1;
